# Data Types In JavaScript 

JavaScript provides different data types to **hold** different types of values. There are two types of data types in JavaScript.
   * 1.Primitive data type
   * 2.Non-Primitive data type
  
# Primitive and Non-Primitive data type 

Data Types | Description | Example
---------- | ----------- | -------
String | represents textual data | "mountblue" , 'JavaScript' etc.
Number | an integer or a floating-point number | 3, 3.234, 3e-2 etc.
Big Int | an integer with arbitrary precision |900719925124740999n , 1n etc.
Boolean | Any of two values: true or false | true and false
Undefined | a data type whose variable is not initialized | let x;
null | denotes a null value | let a = null;
Symbol (ES6) | data type whose instances are unique and immutable introduced in ES6 update | let value = Symbol('hello');
Object | key-value pairs of collection of data | let student = { };

## String 

**String** is used to store text.

* String Syntax
 ``` javascript 
const name = 'ram';
const name1 = "hari";
const result = `The names are ${name} and ${name1}`;

```
## Number 

**Number** represents integer and floating numbers (decimals and exponentials).

* Number Syntax
 ``` javascript 
const number1 = 3/0;
console.log(number1); // Infinity

const number2 = -3/0;
console.log(number2); // -Infinity

const number3 = "abc"/3; 
console.log(number3);  // NaN

```
## BigInt
**BigInt** number is created by appending n to the end of an integer.

* BigInt Syntax
 ``` javascript 
// BigInt value
const value1 = 900719925124740998n;

// Adding two big integers
const result1 = value1 + 1n;
console.log(result1); // "900719925124740999n"

const value2 = 900719925124740998n;

// Error! BitInt and number cannot be added
const result2 = value2 + 1; 
console.log(result2); 

```
## Boolean
This data type represents logical entities. **Boolean** represents one of two values: true or false.

* Boolean Syntax
 ``` javascript 
const dataChecked = true;
const valueCounted = false;

```
## Undefined 
The **undefined** data type represents value that is not assigned.

* Underfined Syntax
 ``` javascript 
let name = undefined;
console.log(name); // undefined

```
## null 
In JavaScript, **null** is a special value that represents empty or unknown value.

* null Syntax
 ``` javascript 
const number = null;

```

## Symbol(ES6)
This data type was introduced in a newer version of JavaScript (from ES2015).

A value having the data type Symbol can be referred to as a **symbol** value. Symbol is a primitive value that is unique.

* Symbol Syntax
 ``` javascript 
// two symbols with the same description

const value1 = Symbol('hello');
const value2 = Symbol('hello');

```
## Object 
An **object** is a complex data type that allows us to store collections of data.

* Object Syntax
 ``` javascript 
const student = {
    firstName: 'ram',
    lastName: null,
    class: 10
};

```

# Data Structures in JavaScript(ES6)
**ES6** introduces two new data structure in javascript in 2015 .
  * **Maps**
  * **Sets**

## Map :
 The Map is data structure which can hold key/value pair ,key and values in a map may be primitive or objects .Maps distinguish between similar values but bear different data types .

### Map methods

Methods | Description
------- | -----------
Map.clear() | Its removes all key/values pairs from map object.
Map.delete(key) | Its removes any value associted to the key.
Map.entries() | Its return new iterator object that conatain an array of [Kye,value] for elements in the Map object in insertion order.
Map.values() | Its returns an object of new Map iterator.

#### Map Syntax with example
``` javascript
var emp = new Map([ 
   ['e1', 'Aditya'], 
   ['e2', 'Aman'], 
   ['e3', 'Raj'], 
]);  
console.log(roles.get('e2'))

```

## Weak Maps :
A **Weak Map** is similar to a map with follwing exception.
  * Its keys must be objects.
  * Keys in a weak map can be Garbage collected.
  * A weak map cannot be iterated or cleared.

#### Syntax for Weak Map

**let weakMap = new WeakMap();**

## Set :
A **Set** data structure allows to add data to a container.ECMAScript 6 (also called ES2015) introduced the Set data structure to the JavaScript world, along with Map.
 It is similar to an array with an exception that it cannot contain duplicates.
 **Sets** can let you store **unique** values. It supports both primitive values and object.

 ### Set Methods

 Methods | Description 
 ------- | -----------
 Set.add() | You can add items to the Set .
 Set.clear() | Its remove all elements from set objects.
 Set.delete(value) | Its remove the element associated to the value.
 set.value() | Returns a new Iterator object that contains the values.

 #### Syntax for Set

 **const s = new Set()**

 ## Weak Set :
 A **WeakSet** is a special kind of Set.**Weak sets** can only contain objects, and the objects they contain may be garbage collected.

 * Here are the main differences:
   * you cannot iterate over the WeakSet.
   * you cannot clear all items from a WeakSet.
   * you cannot check its size.
  
#### Syntax Weak Set :
**let weakSet = new WeakSet();**











